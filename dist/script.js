//THREE.js
let clock = new THREE.Clock();
let delta = clock.getDelta();
let moveDistance = delta*200;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

renderer.setClearColor( 0x454545 );

var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var charGeometry = new THREE.BoxGeometry( 1, 2, 1 );

var material = new THREE.MeshLambertMaterial( 0x000000 );

var char = new THREE.Mesh( charGeometry, material );
scene.add( char );
char.position.x = 2.5;

camera.position.set(1,2,8)

var light = new THREE.PointLight( 0x008b8b );
light.position.set( 5, 10, 25 );
scene.add( light );

var leftLight = new THREE.PointLight( 0x00ffff );
light.position.set( 25, 10, 25 );
scene.add( leftLight );

var rightLight = new THREE.PointLight( 0xe0ffff );
light.position.set( -25, 10, 25 );
scene.add( rightLight );

var size = 30;
var divisions = 10;

var gridHelper = new THREE.GridHelper( size, divisions );
gridHelper.position.x = 2.5;
gridHelper.position.y = -1;
gridHelper.position.z = -29.5;
scene.add( gridHelper );
gridHelper.scale.x = .107;

let left;
let right;

function move(e) {
  if(left){
    if(char.position.x > 0){
        camera.translateX();
        camera.position.x += -.1;        
        char.position.x += -.1;
      }
  }
  if(right){
    if(char.position.x < 5){
        camera.position.x += .1;
        char.position.x += .1;
      }
  }
}

window.addEventListener( 'keydown', function(e){
    if(e.key === 'z'){
      left = true;
    }
    if(e.key === 'x'){
      right = true;
    }
    e.preventDefault();
  }, false );

window.addEventListener( 'keyup', function(e){
    if(e.key === 'z'){
      left = false;
    }
    if(e.key === 'x'){
      right = false;
    }
    e.preventDefault();
  }, false );

var box = new THREE.Mesh( geometry, material );

let lost = false;
let intervalTime = 3000;
let level = 1;
let boxCounter = 0;

function setGame(){
  window.removeEventListener('click', setGame);
  
  for(let j = 1; j < 4; j++){
    const life = document.getElementById('life' + j);
    life.style.display = 'inline-block';
  }
 
  const lostTitle = document.getElementById('lost');
  lostTitle.style.display = 'none'; 
  lost = false;
  
  spawnBox(intervalTime);
}

setGame();

function spawnBox(interval){
  let spawnInterval = setInterval(function(){
    if(lost){
      const lostTitle = document.getElementById('lost');
      lostTitle.style.display = 'block'; 
      clearInterval(spawnInterval);
      window.addEventListener('click', setGame, false);
    } else if (boxCounter === 5) {
      boxCounter = 0;
      level++;
      intervalTime -= 50;
      spawnBox(intervalTime);
      const levelTitle = document.getElementById('level');
      levelTitle.innerHTML = '<h1>LEVEL: ' + level + '</h1>'; 
      clearInterval(spawnInterval);
    } else {
      boxCounter++;
      console.log(boxCounter);
      scene.add( box );
      box.position.z = -30;
      box.position.x = Math.random()*5.5;
    }
  }, interval);
}

let lifeCounter = 3;

function removeLives(){
  if(box.position.z <= -(level/2) && box.position.z >= -(level*2)/3){
    if(char.position.x + .5 > box.position.x - .5 && box.position.x + .5 > char.position.x - .5){
      const life = document.getElementById('life' + lifeCounter);
      life.style.display = 'none';
      lifeCounter--;
      if(lifeCounter === 0){
        lost = true;
        lifeCounter = 3;
      }
    }
  }
}

function animate() {
  requestAnimationFrame( animate );
  
  box.position.z += .2*level;
  removeLives();
  move();
  
  camera.updateProjectionMatrix();
  renderer.render(scene, camera);
}

window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize(){
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}

animate();